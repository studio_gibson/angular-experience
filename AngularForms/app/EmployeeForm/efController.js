angularFormsApp.controller('efController', function efController($scope,  $window, $routeParams, DataService, $http/*, $modalInstance*/) {

    if ($routeParams.id)
        $scope.employee = DataService.getEmployee($routeParams.id);
    else
        $scope.employee = { id: 0 };

    $scope.editableEmployee = angular.copy($scope.employee);
    $scope.departments = [ "Engineering",
                           "Marketing",
                           "Finance",
                           "Administrations"];
    $scope.programmingLanguages = [
        "C",
        "C++",
        "C#",
        "JavaScript",
        "Java",
        "Pascal",
        "Perl",
        "PHP"
    ];
    
    $scope.hoveringOver = function (value) {
        $scope.overStar = value;
        $scope.percent = 100 * (value / 10);
    }

    $scope.shouldShowFullName = function () {
        return true;
    }

    $scope.getLocation = function (val) {
        return $http.get('//maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                sensor: false
            }
        }).then(function (response) {
            //return response.data.results.map(function (item) {
            //    return item.formatted_address;
            //})
            return response.data.results;
        }
        );
    }
    $scope.submitForm = function () {

        $scope.$broadcast('show-errors-event');

        if ($scope.employeeForm.$invalid)
            return;

        if ($scope.editableEmployee.id == 0) {
            // insert a new employee
            DataService.insertEmployee($scope.editableEmployee);
        }
        else {
            // update the employee
            DataService.updateEmployee($scope.editableEmployee);
        }
        $scope.employee = angular.copy($scope.editableEmployee);
        $window.history.back();
        //$modalInstance.close();
    };
    $scope.cancelForm = function () {
        $window.history.back();
        //$modalInstance.dismiss();
    };
    $scope.resetForm = function () {
        $scope.$broadcast('hide-errors-event');
    }
});
