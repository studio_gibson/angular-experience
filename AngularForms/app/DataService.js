angularFormsApp.factory('DataService', function () {
    var getEmployee = function (id) {
        if (id == 123) {
             return {
                    fullName: "Milton Waddams",
                    notes: "The ideal employee. Just don´t touch his red staples",
                    department: "Administration",
                    dateHired: "July 11 2015",
                    breakTime: "July 11 2015 11:00 PM",
                    topProgrammingLanguage: "",
                    perkCar: true,
                    perkStock: false,
                    perkSixWeeks: true,
                    payrollType: "none"
            }
        }
        return undefined;
    };

    var insertEmployee = function (newEmployee) {
        return true;
    };

    var updateEmployee = function (employee) {
        return true;
    };
    return {
        insertEmployee: insertEmployee,
        updateEmployee: updateEmployee,
        getEmployee: getEmployee
    };
});

 